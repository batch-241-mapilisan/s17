function displayUserInformation() {
    let fullName = prompt("What is your full name?");
    let age = prompt("What is your age?");
    let location = prompt("What is your location?");
    alert("Thank you for your input.");
    console.log(`Hello, ${fullName}`);
    console.log(`You are ${age} years old`);
    console.log(`You live in ${location}`);
}

displayUserInformation();

function displayTop5MusicalArtists() {
    let band1 = prompt("Please input one of your top 5 band/musical artist.");
    let band2 = prompt("Please input one of your top 5 band/musical artist.");
    let band3 = prompt("Please input one of your top 5 band/musical artist.");
    let band4 = prompt("Please input one of your top 5 band/musical artist.");
    let band5 = prompt("Please input one of your top 5 band/musical artist.");

    console.log(`1. ${band1}`);
    console.log(`2. ${band2}`);
    console.log(`3. ${band3}`);
    console.log(`4. ${band4}`);
    console.log(`5. ${band5}`);
}
displayTop5MusicalArtists();

function displayTop5Movies() {
    let movie1 = prompt("Please input one of your top 5 movie");
    let movie1Rating = prompt("Please input its Rotten Tomato Rating");
    let movie2 = prompt("Please input one of your top 5 movie");
    let movie2Rating = prompt("Please input its Rotten Tomato Rating");
    let movie3 = prompt("Please input one of your top 5 movie");
    let movie3Rating = prompt("Please input its Rotten Tomato Rating");
    let movie4 = prompt("Please input one of your top 5 movie");
    let movie4Rating = prompt("Please input its Rotten Tomato Rating");
    let movie5 = prompt("Please input one of your top 5 movie");
    let movie5Rating = prompt("Please input its Rotten Tomato Rating");

    console.log(`1. ${movie1}`);
    console.log(`Rotten Tomatoes Rating: ${movie1Rating}`);
    console.log(`2. ${movie2}`);
    console.log(`Rotten Tomatoes Rating: ${movie2Rating}`);
    console.log(`3. ${movie3}`);
    console.log(`Rotten Tomatoes Rating: ${movie3Rating}`);
    console.log(`4. ${movie4}`);
    console.log(`Rotten Tomatoes Rating: ${movie4Rating}`);
    console.log(`5. ${movie5}`);
    console.log(`Rotten Tomatoes Rating: ${movie5Rating}`);
}
displayTop5Movies();

alert("Hi please add your friends");

function displayTop3Friends() {
    let friend1 = prompt("Please input one of your friends name")
    let friend2 = prompt("Please input one of your friends name")
    let friend3 = prompt("Please input one of your friends name")

    console.log("You are friends with:");
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
}

displayTop3Friends();
